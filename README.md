# TomEE-server
Création d'un serveur TomEE dans un conteneur LXC avec Ansible

Fonctionnalités et choses à savoir :
* Nous sommes partis du projet Jenkins, nous avons donc laissé les fichiers mais n'utilisons pas le rôle
* On suppose évidemment qu'un container LXC est djà créé (via le premier script par exemple)
* Le fichier _inventory.ini_ est à modifier selon l'IP de son container LXC

# Membres du groupe
Projet effectué par :
* Maxime Chevallier-Pichon
* Gabriel Fruhauf